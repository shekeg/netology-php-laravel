<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'contacts')->name('index');

Route::resource('contacts', 'ContactsController', ['except' => ['show']]);

Route::get('contacts/find', 'ContactsController@find')->name('contacts.find');