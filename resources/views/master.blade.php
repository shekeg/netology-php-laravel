<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <style>
    table {
      border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid black;
    }
  </style>
</head>
<body>
  <h1>Записаня книжка</h1>
  <ul>
    <li>
      <a href="{{ route('contacts.create') }}">Новый контакт</a>
    </li>
    <li>
      <a href="{{ route('index') }}">Список контактов</a>
    </li>
  </ul>
  @yield('content')
</body>
</html>