@extends('master')

@section('content')
  <h2>Добавить контакт</h2>
    @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div>
  @endif
  @isset ($contact)
    <form action="{{ route('contacts.update', $contact->id) }}" method="POST">
      @csrf
      {{method_field('PUT')}}
      <input type="text" name="name" value="{{ $contact->name }}" autocomplete="off">
      <input type="text" name="phone_number" value="{{ $contact->phone_number }}" autocomplete="off">
      <input type="submit" value="Сохранить">
    </form>
  @endisset
  @empty ($contact)
    <form action="{{ route('contacts.store') }}" method="POST">
      @csrf
      <input type="text" name="name" autocomplete="off">
      <input type="text" name="phone_number" autocomplete="off">
      <input type="submit" value="Добавить">
    </form>
  @endempty
@endsection