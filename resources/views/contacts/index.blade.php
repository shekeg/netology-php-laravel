@extends('master')

@section('content')
  <h2>Поиск по записям</h2>
  <form action="{{ route('contacts.find') }}" method="GET">
    @csrf
    <input type="text" name="find_query" value="{{ $findQuery or ''}}" autocomplete="off">
    <input type="submit" value="Найти">
    @isset ($isFilter)
      <a href="{{ route('index') }}">Сбросить фильтр<a>
    @endisset
  </form>
  <h2>Список контактов</h2>
  <table>
    <thead>
      <tr>
        <td>№</td>
        <td>ФИО</td>
        <td>Номер телефона</td>
        <td>Настроить</td>
      </tr>
    </thead>
    <tbody>
      @foreach( $contacts as $contact)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $contact->name }}</td>
        <td>{{ $contact->phone_number }}</td>
        <td>
          <form action="{{ route('contacts.destroy', $contact->id) }}" method="POST">
            <a href="{{ route('contacts.edit', $contact->id) }}">Изменить</a>
            @csrf
            {{ method_field('DELETE') }}
            <button type="submit">Удалить</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection