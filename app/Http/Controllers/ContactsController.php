<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $contacts = Contact::orderBy('name')->get();
      return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('contacts.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $validate = $request->validate([
        'name' => 'required',
        'phone_number' => 'required'
      ]);
      Contact::create($request->only('name', 'phone_number'));
      return redirect()->route('index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
      return view('contacts.form', compact('contact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
      $validate = $request->validate([
        'name' => 'required',
        'phone_number' => 'required'
      ]);
      $contact->update($request->only('name', 'phone_number'));
      return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        return redirect()->route('index');
    }

    public function find(Request $request) {
      $findQuery = $request->find_query;
      $contacts = Contact::orderBy('name')
                    ->where('name', 'LIKE', '%' . $findQuery . '%')
                    ->orWhere('phone_number', 'LIKE', '%' . $findQuery . '%')
                    ->get();
      return view('contacts.index', [
        'findQuery' => $findQuery,
        'contacts' => $contacts,
        'isFilter' => true
      ]);
    }
}
